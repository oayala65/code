# Temperature in Celsius
C = 30

#=
The following code below calculates the Fahrenheit
from Celsius 
=#
F = ((9 / 5) * C) + 32

println(30, " C = ", F, " F")
